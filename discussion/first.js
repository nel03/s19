// console.log('Hi')

/*
	selection control structures
		>it sorts out wheter the statements are to be executed based on the condition whether it's true or false

		>two-way selection (ture or false)
		> multi-way selection
*/

/*
	if else statement

	syntax: 
		if (condition){
			statement
		} else if (condition){
			statment palced if statement did not work
		} else {
			statement last condition if and else if did not work
		}

		if statement - execute a statement if a specified condition is true
*/

let numA = -1;

if (numA < 0){
	console.log ("hellow");
}

console.log(numA < 0)

let city = "New York";

if (city === 'New York'){
	console.log ('welcome to new york city!');
};

console.log(city === 'New York')

/*
	else if
		> executes a statement if our previous conditions are false and if the specified condition is true
		> the else if clause is optional and can be added to capture additional conditions to change the flow of a program
*/

let numB = 1;

if (numA > 0 ){
	console.log("This is the if Statement");
} else if (numB > 0) {
	console.log("This is the else if statement")
}

city = "Tokyo"

if (city === 'New york'){
	console.log('this is the if string statement')
} else if (city === 'Tokyo'){
	console.log('This is the else if string statement');
};

/*
	else statement
		> executes a statement if all of our previous conditions are false
*/

if (numA > 0 ){
	console.log("This is the if Statement");
} else if (numB === 0) {
	console.log("This is the else if statement")
} else {
	console.log('else is the last statement')
}


// let age = parseInt(prompt('Enter your Age: '));

// if (age <= 18){
// 	console.log('Drink only milk');
// } else {
// 	console.log('Lets Drink Gin together');
// };

/*
	mini activity
*/

// let height = parseInt(prompt('Enter your height'));

// if (height < 150){
// 	console.log ('Drink more milk');
// } else {
// 	console.log('Drink Gin you passed');
// };

// or

// function getHeight(height) {
// 	if (height <= 150){
// 		console.log('Did not pass the minimum height requirement')
// 	} else{
// 		console.log('You passed drink more Gin');
// 	};

// let height = parseInt(prompt('Enter your height'));
// getHeight(height);

// let isLegalAge = true;

// let isAdmin = false;

// if (isLegalAge){
// 	if(!isAdmin){
// 		console.log('you are not an admin')
// 	}
// };


let message = "No message"

function determinTyphoonIntensity(windSpeed){
	if (windSpeed < 30){
		return 'not a Typhoon';
	} else if (windSpeed <= 61){
		return 'Tropical depression detected';
	} else if (windSpeed >= 62 && windSpeed <= 88){
		return 'Tropical storm detected'
	} else if (windSpeed >= 89 && windSpeed <= 177) {
		return 'Severe tropical storm detected'
	} else {
		return ' Typhoon Detected'
	}
}

message = determinTyphoonIntensity(70);
console.log(message);


/*
	truthy and falsy
		> in Js a truthy value is considered true when encountered in a boolen context
		
		>falsy values
		1. false
		2. 0
		3. -0
		4. empty
		5. null
		6. undefined
		7. NaN
*/

// truthy values
let word = 'true';

if (word){
	console.log('Truthy');
};

if (true){
	console.log('Truthy2');
};

if (1){
	console.log('Truthy3');
};

//falsy value - it will not invoked or shown

if(0){
	console.log('none')
}

/*
	conditional (ternary) operator - for short codes

	ternary operator takes in three operands
	1. condition
	2. expression to execute if the condition is true
	3. expression to execute if the condition is false

	syntax:
	(condition) ? ifTrue_expression :
	ifFalse_expression
*/

//single statement execution
let ternaryResult = (1 < 18) ? "condition is True" : "condition is False";

console.log("Result of the Ternary Operator:");
console.log(ternaryResult);

//multiple statement execution

// let name;

// function isOfLegalAge(){
// 	name = 'John';
// 	return 'You are the of the legal age limit';
// }

// function isUnderAge(){
// 	name = 'Jane';
// 	return 'You are under the age limit';
// };

// let age = parseInt(prompt('what is your age'));

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
// console.log('Result of ternary operator in function:')
// console.log (legalAge + ',' + name);


/*
	switch statement
		> can be used as an alternative to an if .. else statement where the date to be used in the condition of an expected input:

		syntax: switch (expression) {
			case <value>:
				statement;
				break;
				default:
				statement;
				break;
		}
*/


// let day = prompt('what day of the week is it today?') . toLowerCase()

// console.log(day);

// switch(day) {
// 	case '1' || 'monday':
// 		console.log('The color of the day is white');
// 		break;
// 	case 'tuesday':
// 		console.log('green');
// 		break;
// 	case 'wensday':
// 		console.log('violet');
// 		break;
// 	case 'thursday':
// 		console.log('blue');
// 		break;
// 	case 'friday':
// 		console.log('orange');
// 		break;
// 	case 'saturday':
// 		console.log('pink');
// 		break;
// 	case 'sunday':
// 		console.log('black');
// 		break;
// 	default:
// 		console.log('input valid day');
// 		break;
// };


/*
	try-catch-finally statement

>try catch is commonly used for error handling
>will still function even if the statement is not complete
*/


function showIntensityAlert(windSpeed){
	try{
		alert(determinTyphoonIntensity(windSpeed))
	}
	catch(error){
		console.log(typeof error);
		console.log(error);
		console.warn(error.message);
	}
	finally{
		alert("intensity updates will show new alert!")
	}
}

showIntensityAlert(1);